# Importar biblioteques
import requests
from bs4 import BeautifulSoup
import sys
import csv
import time


# Creació d'un arxiu per escriure-hi dins
f = csv.writer(open("artistes_z.csv", "w"))
f.writerow(["Nom", "Enllaç"])

pagines = []

for pagina in range(1, 5):
    url = (
        "https://web.archive.org/web/20121007172955/https://www.nga.gov/collection/anZ"
        + str(pagina)
        + ".htm"
    )
    pagines.append(url)


for item in pagines:
    # Creació de l'objecte de BeautifulSoup a partir del request inicial
    pagina = requests.get(item)
    soup = BeautifulSoup(pagina.text, "html.parser")

    # Eliminar enllaços innecessàris
    enllaços_finals = soup.find(class_="AlphaNav")
    enllaços_finals.decompose()

    # Extracció d'informació
    div_llistat_artistes = soup.find(class_="BodyText")
    enllaços_llistat_artistes = div_llistat_artistes.find_all("a")

    # Comprovador de que hem extret la informació necessària
    for artista in enllaços_llistat_artistes:
        nom = artista.contents[0]
        enllaç = "https://web.archive.org" + artista.get("href")

        # Afegir al CSV
        f.writerow([nom, enllaç])

    # Afegit un 'descans' manual
    time.sleep(3)
